import scala.util.Random

1

var res0: Int = 1

res0 = 2

val y = (res0 = 3)

val z = {
  res0 = 6
  val x = 3
}

var o: Unit = println("Hello")

class One {
  private val value: Int = 1 + 1
  val t = One.value
}

object One {
  private val value: Int = 1 + 1
  var v2: Int = 1
}

val one: One.type = One
val two: One.type = One

class MyDog(nameArg: String) {
  val name: String = "My " + nameArg
}

object Sounds {
  def meow(): Unit = {
    val wow: String = "wow!" // Утверждение
    println("meow!") // Утверждение, поэтому возвращаемый тип метода должен быть Unit
  }

  def bark(): String = {
    "bark"
  }
}

Sounds.bark()


def square(x: Int): Int = x * x

val squareFunction: Int => Int =
  square

def sum: (Int, Int) => Int = (x, y) => { x + x + y }
((x: Int, y: Int) => { x + y }).apply(3, 5)

// `square _` references function as a value
val squareFunction: Int => Int = square _

def calculate(a: Int, b: Int, op: Int => Int => Int): Int =
  op(a)(b)

def sum(a: Int, b: Int): Int = a + b
def multiply(a: Int, b: Int): Int = a * b

val resultOne = calculate(1, 2, y => sum(_, y))      // 1 + 2 = 3

class G {
  var global: Int = 10
  def sum(a: Int, b: Int): Int = a + b + global

}

val g = new G()

val f = (a, b) => g.sum(a, b)



def factorial(n: Int, acc: Int = 1): Int =
  if (n < 1) acc else factorial(n - 1, acc * n)

val r = factorial(100000000)


val x: Int = Random.nextInt(10)

val result: String = x match {
  case 100 => "1"
  case i if i % 2 == 0 => "even"
  case _ => "odd"
}

def prettyPrint(in: Int)(f: Int => String): Unit =
  println(f(in))


prettyPrint(in = Random.nextInt(10))({ x =>
  x match {
    case x if x % 2 == 0 => "even"
    case _               => "odd"
  }
})
prettyPrint(Random.nextInt(10)) { x =>
  x match {
    case x if x % 2 == 0 => "even"
    case _               => "odd"
  }
}
prettyPrint(Random.nextInt(10)) {
  case x if x % 2 == 0 => "even"
  case _               => "odd"
}

trait Named {
  val name: String
}

trait Meowing {
  def meow(): Unit
}

class Cat(override val name: String) extends Named with Meowing {
  override def meow(): Unit = println("meow!")
}

final case class CompoundKey(partition: Int, key: String)

val key: CompoundKey = CompoundKey(5, "asdasdasd")
key match {
  case a@CompoundKey(p, k) if p.isInstanceOf[Int] =>
    a.toString
    println(s"partition $p, key: $k")
}

val `TINKOFF 123`: CompoundKey = CompoundKey(1, "test")
val CompoundKey(partion, key) = `TINKOFF 123`
println(partion) // Напечатает 1
println(key)     // Напечатает "test"
