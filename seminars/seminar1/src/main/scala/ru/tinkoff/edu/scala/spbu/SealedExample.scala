package ru.tinkoff.edu.scala.spbu

sealed trait SealedExample {

}

sealed trait Status
object Created extends Status
object Deleted extends Status
