package ru.tinkoff.edu.scala.spbu

import scala.annotation.tailrec

object Main {
  @tailrec
  def factorial(n: Int, acc: Int = 1): Int =
    if (n < 1) acc else factorial(n - 1, acc * n)


  def main(args: Array[String]): Unit = {
    println(factorial(100000))
  }

  trait A {
    val asd = 12
  }

  trait A1 extends A

  trait A2 extends A

  trait B {
    this: A =>
  }

  object T extends A2 with B

  def squeeze(a: Int, b: Int)(f: ((Int, Int)) => Int): Int =
    f((a, b))

  val res2 = squeeze(1, 2) {
    case (a, b) if a == 0 => b
    case (a, b) => a + b
  }

  val res = squeeze(1, 2) { (x: (Int, Int)) =>
    x match {
      case (a, b) => a + b
    }



  }

  def func(x: (Int, Int)): String = {
    case (a, b) => "asd"
  }
}

class FuncVisibility {
  private var i = 123

  private val j = i

  val func2: (Int, S1) => String = {
    case (a, P1(3, c)) => s"asd $i $c"
    case (a, P1(b, c)) if b % 2 == 0 => s"asd $i $b $c"
    case (u, _ : P1) if u == i => s"asd $i something else"
    case (a, _ : O.type) => s"asd $i something else"
  }

}


final case class P1(i1: Int, i2: Int) extends S1
final object O extends S1

sealed trait S1

case class A(i: Int) extends S1

case class B(i: Int) extends S1

