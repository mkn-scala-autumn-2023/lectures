import scala.annotation.nowarn

@nowarn
class PureFunctions {

  def parseRate(raw: String): Int = ???
  def invert(rate: Int): Int = ???
  val globalFlag: Boolean = ???
  def reportMessage(rate: Int): Unit = ???
  def saveToDatabase(rate: Int): Unit = ???

  def processMessage(raw: String): Unit = {
    val rate = parseRate(raw)
    val strongRate =
      if (rate < 1) invert(rate) else rate
    if (globalFlag)
      reportMessage(strongRate)
    else
      saveToDatabase(strongRate)
  }

  trait Printable[A] {
    def format(value: A): String
  }

  object Printable {
    implicit val intPrintable: Printable[Int] = (value: Int) => value.toString
    implicit val bigDecimalPrintable: Printable[BigDecimal] =
      _.toString
//    implicit val stringPrintable: Printable[String] =
//      identity

  }

  class IntPrintable extends Printable[Int] {
    override def format(value: Int): String = value.toString
  }

  def print[A](s: String, input: A)(implicit p: Printable[A]): Unit =
    println(p.format(input))

  class Person(
                val name: String,
                val age: Int,
                val money: BigDecimal
              )
  object Person {
    implicit def personPrintable(implicit
                                 intPrintable: Printable[Int],
                                 bigDecimalPrintable: Printable[BigDecimal],
                                 stringPrintable: Printable[String] = identity
                                ): Printable[Person] = { person =>
      val name = stringPrintable.format(person.name)
      val age = intPrintable.format(person.age)
      val money = bigDecimalPrintable.format(person.money)
      s"Person($name, $age, $money)"
    }
  }

  def main(args: Array[String]): Unit = {
    val person = new Person("Ivan", 23, -1)
    println(implicitly[Printable[Person]].format(person))

    val objPrintable: Printable[Object] = _.toString

    print("", new Object)(objPrintable)

    val result = implicitly[Printable[Int]].format(1) // result = "1"

    trait MyTypeClass[T]

    case class Wrapper[T]( value: T)
    object Wrapper {
      implicit def derivePrintable[A, T: Printable]: Printable[Wrapper[T]] = { v =>
        // для T есть неявное значение Printable[T]
        implicitly[Printable[T]].format(v.value)
      }
    }}


}
