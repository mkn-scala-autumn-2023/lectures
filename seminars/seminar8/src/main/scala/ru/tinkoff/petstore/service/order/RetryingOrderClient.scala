package ru.tinkoff.petstore.service.order

import cats.MonadThrow
import cats.effect.kernel.Async
import retry.{Sleep, retryingOnSomeErrors}
import ru.tinkoff.petstore.client.order.OrderClient
import ru.tinkoff.petstore.client.order.model.request.CreateOrderRequest
import ru.tinkoff.petstore.client.order.model.response.OrderResponse
import ru.tinkoff.petstore.commons.RetryUtils

import java.util.UUID

class RetryingOrderClient[F[_]: MonadThrow : Sleep](
    orderClient: OrderClient[F],
    retryUtils: RetryUtils[F],
) extends OrderClient[F] {
  override def createOrder(request: CreateOrderRequest): F[OrderResponse] =
    retryingOnSomeErrors[OrderResponse](
      isWorthRetrying = retryUtils.isTimeoutException,
      policy = retryUtils.policy,
      onError = retryUtils.onError,
    )(orderClient.createOrder(request))

  override def findOrder(orderId: UUID): F[Option[OrderResponse]] =
    retryingOnSomeErrors[Option[OrderResponse]](
      isWorthRetrying = retryUtils.isTimeoutException,
      policy = retryUtils.policy,
      onError = retryUtils.onError,
    )(orderClient.findOrder(orderId))
}
