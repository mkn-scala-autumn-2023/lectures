package ru.tinkoff.effects

import cats.effect.kernel.Clock
import cats.effect.std.{Random, UUIDGen}
import cats.effect.{IO, IOApp, Resource, Temporal}
import cats.implicits.catsSyntaxFlatMapOps

import java.io.{BufferedReader, File, FileOutputStream, InputStreamReader}
import java.net.{HttpURLConnection, URL}
import scala.concurrent.duration.{DurationInt, FiniteDuration}

object Main extends IOApp.Simple {
  override def run: IO[Unit] = Random
    .scalaUtilRandom[IO]
    .flatMap(implicit r =>
      for {
        conn <- acquireConnection
        reader = new BufferedReader(new InputStreamReader(conn.getInputStream))
        s <- read(reader)
        _ <- IO.println(s)
      } yield (),
    )

  def read(in: BufferedReader): IO[String] = IO.interruptible {
    val content = new StringBuffer
    var inputLine = in.readLine
    while (inputLine != null) {
      content.append(inputLine)
      inputLine = in.readLine
    }
    content.toString
  }

  def resourceExample: IO[String] =
    (for {
      c <- Resource.make(acquireConnection)(c =>
        IO.blocking(c.disconnect()).handleErrorWith(_ => IO.unit),
      )
      is <- Resource.fromAutoCloseable(IO.blocking(c.getInputStream))
      isr <- Resource.fromAutoCloseable(IO.delay(new InputStreamReader(is)))
      r <- Resource.fromAutoCloseable(IO.delay(new BufferedReader(isr)))
      _ <- Resource.fromAutoCloseable(IO.blocking(new FileOutputStream(new File(""))))
    } yield r)
      .use(bufferedReader => read(bufferedReader))

  def acquireConnection: IO[HttpURLConnection] = {
    val url = new URL("http://ifconfig.co")
    val connection = url.openConnection.asInstanceOf[HttpURLConnection]
    IO.blocking(connection.connect())
      .as(connection)
  }

  val fibersExample: IO[Unit] =
    for {
      f1 <- traverseExample("fiber 1: ").start
      f2 <- traverseExample("fiber 2: ").start
      _ <- IO.sleep(200.microseconds)
      _ <- f1.cancel
      _ <- f2.join
      _ <- scheduleExample(IO.realTimeInstant.flatMap(i => IO.println(s"started at $i")), 1.seconds)
    } yield ()

  def scheduleExample2(action: IO[Unit], duration: FiniteDuration): IO[Unit] =
    for {
      _ <- IO.sleep(duration)
      _ <- action
      r <- scheduleExample2(action, duration)
    } yield r

  def scheduleExample[F[_]: Temporal](action: F[Unit], duration: FiniteDuration): F[Unit] =
    (Clock[F].sleep(duration) >> action).foreverM

  def traverseExample(marker: String = ""): IO[List[Unit]] = {
    import cats.implicits.toTraverseOps

    (1 to 1_000).toList
      .map(i => (1 to i).sum)
      .map(marker + _)
      .traverse(_ => IO.unit)
  }

  def traverseExample2(): IO[List[String]] = {
    import cats.implicits.toTraverseOps

    (1 to 10_000).toList
      .traverse(i =>
        IO.delay((1 to i).map(_.toString).reduce[String] { case (a: String, b: String) =>
          (a + b).substring(0, 2)
        }),
      )
  }

  def parTraverseExample(): IO[List[String]] = {
    import cats.implicits.catsSyntaxParallelTraverse1

    (1 to 10_000).toList
      .parTraverse(i =>
        IO.delay((1 to i).map(_.toString).reduce[String] { case (a, b) => (a + b).substring(0, 2) }),
      )
  }

}
