
ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"

val catsVersion = "2.10.0"
val catsEffect3 = "3.5.2"

lazy val root = (project in file("."))
  .settings(
    libraryDependencies ++= Seq(
      // cats
      "org.typelevel" %% "cats-core" % catsVersion,
      "org.typelevel" %% "cats-effect" % catsEffect3,
    ),
    name := "pets-store",
  )
  .enablePlugins(JavaAppPackaging)
  .settings(
    Compile / run / fork := true
  )
