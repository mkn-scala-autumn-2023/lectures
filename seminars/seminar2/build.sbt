ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.11"

lazy val root = (project in file("."))
  .settings(
    name := "ab-scala",
      libraryDependencies ++=
        Seq(
            "org.scala-lang" % "scala-reflect" % "2.13.11",
            "org.typelevel" %% "cats-core" % "2.10.0"
        )
  )
