//region type erasure

def matchList[T](list: List[T]): String =
  list match {
    case _: List[String] => "List of strings"
    case _: List[Int] => "List of integers"
    case _ => "List of something"
  }

matchList(List(1, 2, 3)) == matchList(List("a", "b", "c"))

//endregion

//region TypeTag

import scala.reflect.runtime.universe._

def matchListV2[T: TypeTag](list: T): String = typeOf[T] match {
  case tag if tag =:= typeOf[List[String]] => "List of strings"
  case tag if tag =:= typeOf[List[Int]] => "List of integers"
  case _ => "List of something"
}

matchListV2(List(1, 2, 3)) == matchListV2(List("1", "2", "3"))

//endregion

//region improved TypeTag matching

def matchListV3[T: TypeTag](list: List[T]): String = list match {
  case _: List[String @unchecked] if typeOf[T] =:= typeOf[String] => "List of strings"
  case _: List[Int @unchecked] if typeOf[T] =:= typeOf[Int] => "List of integers"
  case _ => "List of something"
}

matchListV3(List(1, 2, 3)) == matchListV3(List("1", "2", "3"))

//endregion

//region variance practice

trait Food
trait Fruit extends Food
trait Apple extends Fruit

trait Producer[T] {
  def produce(): T
}

trait Consumer[T] {
  def consume(t: T): Unit
}

class FruitsProducer extends Producer[Fruit] {
  override def produce(): Fruit = ???
}

class FruitsConsumer extends Consumer[Fruit] {
  override def consume(t: Fruit): Unit = ???
}

//def logic(producer: Producer[Food], consumer: Consumer[Food]): Unit = ???
//def logic(producer: Producer[Food], consumer: Consumer[Fruits]): Unit = ???
//def logic(producer: Producer[Food], consumer: Consumer[Apple]): Unit = ???


//logic(new FruitsProducer, new FruitsConsumer)

//endregion

//region type class

sealed trait Json
final case class JsObject(get: Map[String, Json]) extends Json
final case class JsString(get: String) extends Json
final case class JsNumber(get: Double) extends Json
case object JsNull extends Json

trait JsonWriter[T] {
  def write(value: T): Json
}

//endregion

//region type class exemplars

final case class Person(name: String, email: String)

// OOP example
//final case class Person(name: String, email: String) {
//  def write(value: Person): Json = ???
//}

object JsonWriterInstances {
  val stringWriter: JsonWriter[String] =
    (value: String) => JsString(value)

  val personWriter: JsonWriter[Person] =
    (value: Person) => JsObject(
      Map(
      "name" -> JsString(value.name),
      "email" -> JsString(value.email)
      )
    )
}

//endregion

//region type class interface

object Json {
  def toJson[A](value: A)(w: JsonWriter[A]): Json =
    w.write(value)
}

import JsonWriterInstances._

Json.toJson(Person("Dave", "dave@example.com"))(personWriter)

//endregion

//region type classes practice

//Определите класс типов Printable[A], содержащий единственный метод — format. format должен принимать значение типа A и возвращать String.

//Создайте объект PrintableInstances, содержащий экземпляры Printable для String и Int.

//Определите объект Printable с двумя обобщёнными интерфейсными методами:

//format — принимает значение типа A и экземпляр Printable для соответствующего типа. Он использует Printable для преобразования A в String.

//print — принимает те же параметры, что и format, но возвращает Unit. Он выводит значение A в консоль, используя println.

//endregion

//region solution

trait Printable[A] {
  def format(value: A): String
}

object PrintableInstances {
  val stringPrintable = new Printable[String] {
    def format(input: String) = input
  }

  val intPrintable = new Printable[Int] {
    def format(input: Int) = input.toString
  }
}

object Printable {
  def format[A](input: A)(p: Printable[A]): String =
    p.format(input)

  def print[A](input: A)(p: Printable[A]): Unit =
    println(format(input)(p))
}

import PrintableInstances._

Printable.print("Hello world!")(stringPrintable)

//endregion

//region cats typeclass example

import cats.Functor

class MyClass[T]

val x: Functor[MyClass] = new Functor[MyClass] {
  override def map[A, B](fa: MyClass[A])(f: A => B) = ???
}

//endregion

//region from previous lecture

def sortList[A](comparator: (A, A) => Boolean)(list: List[A]): List[A] =
  list.sortWith(comparator)

val sortInt: List[Int] => List[Int] = sortList[Int](_ < _)

sortInt(List(2, 1, 3))
sortInt(List(5, 2, 1, 3, 1))

def sortListWithTypeclass[A: Ordering](list: List[A]): List[A] = list.sorted

sortListWithTypeclass(List(2, 1, 3))
//endregion

