CREATE TABLE pet (
	id          uuid PRIMARY KEY,
	name        VARCHAR(256),
	category    VARCHAR(256),
	description text
)