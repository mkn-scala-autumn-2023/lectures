package ru.tinkoff.petstore.pets


import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import ru.tinkoff.petstore.domain.order.Order
import ru.tinkoff.petstore.repository.inmemory.OrderRepositoryInMemory
import ru.tinkoff.petstore.service.OrderService

import java.time.{Clock, Instant}

// FunSuite testing style
class OrderServiceSuite extends AnyFunSuite with Matchers with MockFactory {

//  test("OrderService should create order with actual time") {
//
//    val time = Instant.now()
//
//    implicit val clock: Clock = mock[Clock]
//    (clock.instant _).expects().returning(time)
//
//    val repository = OrderRepositoryInMemory()
//    val service = new OrderService(repository)
//    val order = service.buyPet(1)
//
//    order shouldBe Order(1, time)
//  }

}
