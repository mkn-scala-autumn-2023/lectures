package ru.tinkoff.petstore

import cats.MonadThrow
import cats.data.OptionT
import cats.effect.std.Env
import cats.effect.{ExitCode, IO, IOApp}
import com.comcast.ip4s.{Host, IpLiteralSyntax, Port}
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Router
import ru.tinkoff.petstore.common.cache.Cache
import ru.tinkoff.petstore.controller.{ExampleController, OrderController, PetController}
import ru.tinkoff.petstore.domain.order.Order
import ru.tinkoff.petstore.domain.pet.Pet
import ru.tinkoff.petstore.repository.inmemory.{OrderRepositoryInMemory, PetRepositoryInMemory}
import ru.tinkoff.petstore.service.{OrderService, PetService, RepositoryOrderService, RepositoryPetService}
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.swagger.bundle.SwaggerInterpreter

import java.util.UUID
import scala.concurrent.duration.DurationInt

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] =
    for {
      orderCache <- Cache.make[IO, UUID, Order]
      petCache <- Cache.make[IO, UUID, Pet]
      endpoints <- IO.delay {
        List(
          ExampleController.make[IO],
          OrderController.make(RepositoryOrderService(OrderRepositoryInMemory(orderCache))),
          PetController.make(RepositoryPetService(PetRepositoryInMemory(petCache))),
        ).flatMap(_.endpoints)
      }
      swagger = SwaggerInterpreter()
        .fromServerEndpoints[IO](endpoints, "pet-store", "1.0.0")
      routes = Http4sServerInterpreter[IO]()
        .toRoutes(swagger ++ endpoints)
      port <- getPort[IO]
      _ <- EmberServerBuilder
        .default[IO]
        .withHost(Host.fromString("localhost").get)
        .withPort(port)
        .withHttpApp(Router("/" -> routes).orNotFound)
        .build
        .use { server =>
          for {
            _ <- IO.println(
              s"Go to http://localhost:${server.address.getPort}/docs to open SwaggerUI. Press ENTER key to exit.",
            )
            _ <- IO.readLine
          } yield ()
        }
    } yield ExitCode.Success

  private def getPort[F[_]: Env: MonadThrow]: F[Port] = {
    OptionT(Env[F].get("HTTP_PORT"))
      .toRight("HTTP_PORT not found")
      .subflatMap(ps => ps.toIntOption.toRight(s"Expected int in HTTP_PORT env variable, but got $ps"))
      .subflatMap(pi => Port.fromInt(pi).toRight(s"No such port $pi"))
      .leftMap(new IllegalArgumentException(_))
      .rethrowT
  }

}
