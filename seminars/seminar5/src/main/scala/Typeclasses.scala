import cats.Monad
import cats.implicits.{catsSyntaxApplicativeId, toFlatMapOps, toFunctorOps}

object Typeclasses {

  final case class Last[+T](value: Option[T])

  final case class Point[+T](x: T, y: T, z: T)

  sealed trait Tree[+T]

  final case class Leaf[+T](value: T) extends Tree[T]

  final case class Branch[+T](value: T, left: Tree[T], right: Tree[T]) extends Tree[T]

  object Nil extends Tree[Nothing]


  def main(args: Array[String]): Unit = {


//    val f1: Int => Int => Int = ???
//    val f2: Int => Int = f1(1)
//
//    def foo[F[_]]: Unit = ???
//
//    type EitherThrow[A] = Either[Throwable, A]
//    foo[Either[Throwable, *]]
//    foo[EitherThrow]
//    foo[({type E[a] = Either[Throwable, a]})#E]
    // ----
//    foo[Either[Throwable, Any]]
//    foo[Either[Throwable, _]]

    trait F1[A, F[_]]

    val f11 = new F1[Int, List] {}
    trait F2[F[_]]

    trait Foo[F[_], Other]

    import cats.Eval

    def factorial(n: BigDecimal): Eval[BigDecimal] =
      if (n > 1)
        Eval.defer(factorial(n - 1).map(_ * n))
      else
        Eval.now(1)

    def loop[F[_] : Monad](in: Long): F[Long] = {
      if (in < 1)
        0L.pure[F]
      else
        in.pure[F].flatMap(i =>
          loop(i - 1).map(_ + i)
        )
    }

    val t: List[Int] = ???
    val t1: Either[String, Int] = ???

//    println(loop[Eval](1000000000L).value)
//    println(factorial(1000000000L).value)

    //    def bar1[F[_]: Foo[*[_], String]]: Unit = ???
//    def bar2[F[_]](implicit a: Foo[F[_], String]): Unit = ???
//    bar1[List]
  }


}
